<?php

require('lib/fpdf.php');

class pedidoPdf extends FPDF
{

    function __construct($rows)
    {
        parent::__construct(); //Es necesario esto
        $this->AddPage();
        $this->tableHeader($rows);
        $this->tableBody($rows);
        $this->Output();
    }
    
    function tableHeader($rows)
    {
     //var_dump($rows);
        $row = $rows[0];
            foreach ($row as $key=> $value)
            {
                $this->SetFont('Arial','B',12);
                $this->setFillColor(200,200,200);
                $this->Cell(30,10,$key,1,"","",true);  
            }
         $this->Ln(10);
    }
    
    function tableBody($rows)
    {
       foreach ($rows as $row){ 
         foreach ($row as $key=> $value)
            {
                $this->SetFont('Arial','B',12);
                $this->Cell(30,10,$value,1);  
            }
             $this->Ln(10);
    }
    }

}
