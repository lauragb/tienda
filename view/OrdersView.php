<?php

require_once 'lib/View.php';
require_once 'lib/Lang.php';

class OrdersView extends View
{ 

        function __construct()
    {
        parent::__construct();
    }
    
    public function render($rows,$template='orders.tpl')
    {
        $this->smarty->assign('rows',$rows);
        $this->smarty->display($template);
    }
    
  
    
 
}
