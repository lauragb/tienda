<?php

require_once 'lib/View.php';
require_once 'lib/Lang.php';

class UserView extends View
{ 

        function __construct()
    {
        parent::__construct();
        //echo ' En la vista Index ';
    }
    
    public function render($rows, $template='user.tpl')
    {

        $this->smarty->assign('rows',$rows);
        $this->smarty->display($template);
    }
    
    public function addForUser()
    {
        $template="userFormAddUser.tpl";
        $this->smarty->display($template);
    }
    
    public function addForAdmin($roles)
    {
        $template="userFormAddAdmin.tpl";
        $this->smarty->assign('roles',$roles);
        $this->smarty->display($template);
    }
    
    public function edit($row,$error="",$roles)
    {
        $template="userFormEdit.tpl";
        $this->smarty->assign('row',$row);
        $this->smarty->assign('roles',$roles);
        $this->smarty->assign('error',$error);
        $this->smarty->display($template);  
    }
    
    
}
