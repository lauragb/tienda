<?php

require_once 'lib/View.php';

class IndexView extends View
{ 

        function __construct()
    {
        parent::__construct();
        //echo ' En la vista Index ';
    }
    
    public function render($plantilla='index.tpl')
    {
           
        if(isset($_SESSION['usuario'])){
            $template = 'indexLogin.tpl';
            $this->smarty->assign('user',$_SESSION['usuario']);
            $this->smarty->display($template); 
        }
        else{
           $this->smarty->assign('method',$this->getMethod());
           $this->smarty->display($plantilla); 
        }
        
    }
    
    
    
}
