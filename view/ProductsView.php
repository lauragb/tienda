<?php

require_once 'lib/View.php';
require_once 'lib/Lang.php';

class ProductsView extends View
{ 

        function __construct()
    {
        parent::__construct();
    }
    
    public function render($rows,$template='products.tpl')
    {
        $this->smarty->assign('js','ajaxProducts.js');
        $this->smarty->assign('rows',$rows);
        $this->smarty->display($template);
    }
  
 
}
