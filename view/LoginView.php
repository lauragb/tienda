<?php

require_once 'lib/View.php';
require_once 'lib/Lang.php';

class LoginView extends View
{ 

        function __construct()
    {
        parent::__construct();
    }
    
    public function render($template='login.tpl',$message="",$user="")
    {
        $this->smarty->assign('message',$message);
        $this->smarty->display($template);
    }
    
    
    
    
}
