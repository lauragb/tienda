CREATE TABLE producto(
	id INT AUTO_INCREMENT PRIMARY KEY,
	codigo VARCHAR(10),
	nombre VARCHAR(50),
	precio FLOAT,
	existencia INT,
	UNIQUE (codigo)
);