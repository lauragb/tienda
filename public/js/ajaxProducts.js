var current_page = 1;
var rol = 0;

$(document).ready(function(){
   
    var peticionAjax = function(number,rol){
        
       var url = "http://localhost/tienda/es/products/ajaxPageData/"+number;       
       $.get(url,function(data){
           data = JSON.parse(data);
           var pages = data[0];
           var rows = data; 
           cargarTabla(rows,rol);
           cargarPaginas(pages);
       },'html').complete(function() {
               loadEvents();
       });
    }
    
 
    
    var cargarTabla = function(rows,rol){
          $("#tBodyList").empty();
           var filas = "";       
           for(var i=1;i<rows.length;i++){ //menos el valor 0 que es la página
               filas = filas + '<tr class="cell" id="row' + rows[i].id +'">';
               filas = filas + '<td class="cell"><input class="cell id" type="text" name="id" value="'+ rows[i].id +'" disabled></td>';
               filas = filas + '<td class="cell escritura"><input class="cell" type="text" name="codigo" value="'+ rows[i].codigo +'" disabled></td>';
               filas = filas + '<td class="cell escritura"><input class="cell" type="text" name="nombre" value="'+ rows[i].nombre +'" disabled></td>';
               filas = filas + '<td class="cell escritura"><input class="cell" type="text" name="precio" value="'+ rows[i].precio +'" disabled></td>';
               filas = filas + '<td class="cell escritura"><input class="cell" type="text" name="existencia" value="'+ rows[i].existencia +'" disabled></td>';
               if (rol<2){
                   filas = filas + '<td class="cell"></td>'; 
               }
               if (rol==2){
                   filas = filas + '<td class="cell"><a class="comprar button" href="#popup1">Comprar</a></td>'; 
               }
               if (rol==3){
                   filas = filas + '<td class="cell"><a class="editar" href="#">Editar</a>';
                   filas = filas + '<a class="borrar" href="http://localhost/tienda/es/products/delete/'+rows[i].id+'">Borrar</a></td>'; 
               }
               filas = filas + '</tr>'; 
           } 
           document.getElementById("tBodyList").innerHTML = filas;
    };
    
    
    var addCarrito = function(e){
        var id = $(e.currentTarget).closest("tr").find(".id"); 
         $.post("http://localhost/tienda/es/products/addToBuy/", id, function(data){
                      
        },'json');
    }
    
    
    var limpiarFila = function(celdas){
         for(var i=0;i<celdas.length;i++){
                       $(celdas[i]).val("");
        }   
        
    }
    
    var cargarPaginas = function(pages){
        $("#paginador").empty();
        var botones = "";
        for(var i=1;i<=pages.pages;i++){
            botones += '<span class="pagina" data-page="'+ i +'">'+ i + '</span>';
        }
        $("#paginador").html(botones);        
    }
    
    
    
    /*Para editar un producto*/
    var editar = function(e){
       var celdas = $(e.currentTarget).closest("tr").find(".escritura").find("input"); /*Guardamos los td con clase escritura del mismo tr, seleccionando los input para cambiarle las propiedades*/
               var id = $(e.currentTarget).closest("tr").find(".id");/*Guardamos la id*/
        
               if ($(e.currentTarget).text()=="Editar"){ /*Comprobamos en que estado se encuentra*/
                 $(e.currentTarget).text("Guardar"); /*Modifica el texto*/
             
                for(var i=0;i<celdas.length;i++){
                   $(celdas[i]).addClass("celdaModificar");
                   $(celdas[i]).removeAttr("disabled");
                }   
               }
               else{
                   var row = {};
                   $(e.currentTarget).text("Editar"); /*Para devolver los valores originales*/
                   row["id"] = $(id).val();
                   for(var i=0;i<celdas.length;i++){
                       row[$(celdas[i]).attr("name")] = $(celdas[i]).val();
                       $(celdas[i]).removeClass("celdaModificar");
                       $(celdas[i]).attr("disabled","disabled");
                  } 

                    $.post("http://localhost/tienda/es/products/update/", row, function(data){
                        
                    },'json');
               } 
    };
    
    /*Para crear un producto*/
    var crear = function(event){
        event.preventDefault();
        
         var celdas = $(event.currentTarget).closest("tr").find(".nuevo").find("input"); 
                 if ($(event.currentTarget).text()=="Nuevo"){ /*Comprobamos en que estado se encuentra*/
                    $(event.currentTarget).text("Crear"); /*Modifica el texto*/
             
                    for(var i=0;i<celdas.length;i++){
                       $(celdas[i]).addClass("celdaCrear");
                       $(celdas[i]).removeAttr("disabled");
                    }   
                 }
               else{
                   var row = {};
                   $(event.currentTarget).text("Nuevo"); /*Para devolver los valores originales*/
                   for(var i=0;i<celdas.length;i++){
                       row[$(celdas[i]).attr("name")] = $(celdas[i]).val();
                       $(celdas[i]).removeClass("celdaCrear");
                       $(celdas[i]).attr("disabled","disabled");
                  } 

                    $.post("http://localhost/tienda/es/products/insert/", row, function(data){
                        peticionAjax(current_page,rol);
                        limpiarFila(celdas);
                    },'json');
               }
    };
    
    var borrar = function(event){
        event.preventDefault();
        var id = $(event.currentTarget).closest("tr").find(".id"); 
        
         $.post("http://localhost/tienda/es/products/delete/", id, function(data){
                        
                    },'json').complete(function() {
               peticionAjax(current_page,rol);
       });
        
    }
    
    
    var loadEvents = function(){ /*Cuando cargue todo el AJAX entonces hará esto*/
        try{
            $(".editar").unbind("click");
            $(".crear").unbind("click");
            $(".borrar").unbind("click");
            $(".pagina").unbind("click");
            $(".comprar").unbind("click");
        }catch(u)
        {}
        $(".editar").on("click", editar);
        $(".crear").on("click", crear);
        $(".borrar").on("click", borrar);
        $(".pagina").on("click", function(e){
            current_page = $(e.currentTarget).attr("data-page");
            peticionAjax(current_page,rol);
        });
        $(".comprar").on("click", addCarrito);
    }
    
    
    
    var filtrarTabla = function (text,number,rol) {
        
        if(text==""){
            peticionAjax(number);
            return true;
        }
        
        var url = "http://localhost/tienda/es/products/ajaxPageFilter/"+number+"/"+text;       
       $.get(url,function(data){
           data = JSON.parse(data);
           var pages = data[0];
           var rows = data; 
           cargarTabla(rows,rol);
           cargarPaginas(pages);
       },'html').complete(function() {
               loadEvents();
       });
    };


    $("#filtrado").keyup(function () {
        filtrarTabla($(this).val(),current_page,rol);
    });
    
    
    
    
    
   var loadDoc = function(number){
      
       peticionAjax(number,rol);
   };

   loadDoc(current_page);
 
    
});
