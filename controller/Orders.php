<?php

require_once 'lib/Controller.php';

class Orders extends Controller
{
    public function __construct() 
    {
       parent::__construct('Orders'); 
       //echo '- Dentro de User '; 
    }
    
    public function index()
    {
        $rows = $this->model->getAll();
        $this->view->render($rows);
    } 
    
    public function getUserOrders($id)
    {
        $rows = $this->model->getUserOrders($id);
        $this->view->render($rows);
    } 
    
    
    public function add()
    {
       
    }
    
    public function newOrder()
    {
        $idUser = $_SESSION['idUser'];
        $fechaPedido = date("Y-m-d H:i:s");
        $id = $this->model->newOrder($idUser,$fechaPedido);
        $productos = $_SESSION['carrito'];
        $this->model->insertOrder($productos,$id);
        unset($_SESSION['carrito']);
        header("Location:/tienda/es/orders/getUserOrders/$idUser" ); 
        
    }
    
     public function detail($id)
    {
       $rows = $this->model->getDetalle($id);
       $this->view->render($rows,"detailOrder.tpl");
    }
    
    
    public function pdf($id)
    {
        $rows = $this->model->getDetalle($id);
        require ('view/pedidoPdf.php');
        $pdf = new pedidoPdf($rows);
    }
    
     public function insert()
    {
        
    }
    
    public function delete()
    {
        
    }
    
    public function edit()
    {
       
    }
    
    public function __call($name, $arguments)
    {
        
        
    }
    
    

    
}
