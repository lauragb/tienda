<?php

require_once 'lib/Controller.php';

class Error extends Controller
{

    public function __construct() 
    {
        parent::__construct('Error'); 
       // echo '-  Dentro de error - ';
    }
    
    public function methodFail()
    {
        echo '- No existe el m&eacute;todo -';
    }
    
    public function index()
    {
        $this->view->render();
        //echo 'Metodo por defecto index';
    }

}



