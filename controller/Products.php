<?php

require_once 'lib/Controller.php';

class Products extends Controller
{
    public function __construct() 
    {
       parent::__construct('Products'); 
    }
    
    public function index()
    {
        $rows = $this->model->getAll();
        $paginas = $this->model->numeroPaginas();
        $this->view->render($rows);
    } 
    
    public function ajaxPageData($pageNumber)
    {
      $rows =  $this->model->ajaxPageData($pageNumber); 
      echo json_encode($rows);  //Para pasar los datos a formato json
    }
    
    public function ajaxPageFilter($pageNumber,$text)
    {
        $rows =  $this->model->ajaxPageFilter($pageNumber,$text); 
        echo json_encode($rows); 
    }
    
    public function addToBuy(){
        $id = $_POST['id'];
        $row = $this->model->get($id);
        if ($row['existencia']>0){
            if(isset($_SESSION['carrito'][$id])){
                $producto = $_SESSION['carrito'][$id];
                $producto['cantidad'] = $producto['cantidad'] + 1;
            }
            else{
                $producto['cantidad'] = 1;
                $producto['id'] = $id;
                $producto['nombre'] = $row['nombre'];
                $producto['precio'] = $row['precio'];
            } 
            $_SESSION['carrito'][$id] = $producto;
            return true;
            
        }
       
        else{
            return false;
        }
            
    }
    
    
    public function add()
    {

    }
    
     public function insert()
    {
       $this->model->insert($_POST);  
         echo 1;
    }
    
    public function delete()
    {
       $this->model->delete($_POST);  
         echo 1; 
    }
    
    public function update()
    {
       $this->model->update($_POST); 
    }
    
    public function __call($name, $arguments)
    {
        
        
    }
    
    

    
}
