<?php

require_once 'lib/Controller.php';

class Index extends Controller
{
    public function __construct() 
    {
       parent::__construct('Index'); 
       //echo '- Dentro de Index '; 
    }
    
    public function __toString()
    {
        return 'INDEX';
    }
    
    public function method()
    {
        $this->view->setMethod('METHOD');
        $this->view->render();
        //echo ' y dentro de method - ';
    }
   
    public function hello($nombre="",$apellido="")
    {
        if (true){
            echo 'Hola '.$nombre.' '.$apellido;
        }
        else{
            throw new Exception;
        }
    }
    
    public function index()
    {
        $this->view->render();
    }
    
    public function pruebaPdf()
    {
        require('lib/fpdf.php');

        $pdf = new FPDF();
        $pdf->AddPage();
        $texto = utf8_decode('¡Hola, Mundo!');
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(40,10,$texto);
        $pdf->Output();

    }
    
}
