<?php

require_once 'lib/Controller.php';

class Carrito extends Controller
{
    public function __construct() 
    {
       parent::__construct('Carrito'); 
    }
    
    public function index()
    {
        $rows = $_SESSION['carrito'];
        $this->view->render($rows);
    } 
    
    
    public function add()
    {

    }
    
     public function insert()
    {
       
    }
    
    public function delete($id)
    {
       unset($_SESSION['carrito'][$id]);
        
       header("Location:/tienda/es/carrito" ); 
    }
    
    public function update($id,$cantidad)
    {
       $total = $this->model->getExistencias($id);
       if(($cantidad <= $total['existencia'])){
           $_SESSION['carrito'][$id]['cantidad'] = $cantidad;
       }
      
        header("Location:/tienda/es/carrito" ); 
    }
    
    public function __call($name, $arguments)
    {
        
        
    }
    
    

    
}
