<?php
require_once 'lib/Controller.php';

class Login extends Controller
{
    public function __construct() 
    {
       parent::__construct('Login'); 
    }
    
    public function index()
    {
        $this->view->render();
    }
    
    public function login()
    {
        $this->view->render();
    }
    
    public function run()
    {
        $nombre = "";
        $password = "";
        $mensaje = "";
        
        if(isset($_POST['nombre']))
        {
            $nombre = $_POST['nombre'];
            $password = $_POST['password'];
            $row = $this->model->validate($nombre,$password);
            
            if(count($row))
            {
                $_SESSION['idRole'] = $row["idRole"];
                $_SESSION['idUser'] = $row["id"];
                $_SESSION['usuario']=$nombre;
                $_SESSION['password']=$password;
                $this->view->render('indexLogin.tpl',"",$nombre);
                header("Location: " . Config::URL );
            }
            else
            {
               $message= 'Usuario o contraseña falsos <br>';
               $this->view->render('login.tpl',$message);
            }
        } 
        
    }
    
    
    public function logout()
    {
       unset($_SESSION);
       session_destroy();
       header("Location:index"); 
        
    }

    
    
    public function add()
    {
        
    }
    
     public function insert()
    {
        
    }
    
    public function delete($number)
    {
        
    }
    
    public function edit($number,$error="")
    {
       
    }
    
    public function __call($name, $arguments)
    {
        
        
    }
    
    public function update()
    {
        
        
    }

    
}
