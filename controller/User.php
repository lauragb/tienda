<?php

require_once 'lib/Controller.php';
require_once 'model/RoleModel.php';

class User extends Controller
{
    public function __construct() 
    {
       parent::__construct('User'); 
       //echo '- Dentro de User '; 
    }
    
    public function index()
    {
        $rows = $this->model->getAll();
        $this->view->render($rows);
    }
    
    
    public function addForUser()
    {
        $this->view->addForUser();
    }
    
    public function addForAdmin()
    {
        $roleModel = new RoleModel();
        $roles = $roleModel->getAll(false);
        $this->view->addForAdmin($roles);
    }
 
    public function insertForUser()
    {
        $row = $_POST;
        $row['password'] = md5($row['password']);
        $row['activacion'] = rand(1,10000000000);
        $this->model->insert($row);
        header('location: ' . Config::URL . $_SESSION['lang'].'/login/login');
    }
    
     public function insertForAdmin()
    {
        $row = $_POST;
        $row['password'] = md5($row['password']);
        $this->model->insert($row,$row['idRole']);
        header('location: ' . Config::URL . $_SESSION['lang'].'/user/index');
    }
    
    public function delete($number)
    {
        $this->model->delete($number);
        header('location: ' . Config::URL . $_SESSION['lang'].'/user/index');
    }
    
    public function edit($number,$error="")
    {
        $roleModel = new RoleModel();
        $roles = $roleModel->getAll(false);
        $row = $this->model->get($number);
        $this->view->edit($row,$error,$roles);
    }
    
    public function __call($name, $arguments)
    {
        
        
    }
    
    public function update()
    {
        $row = $_POST;
        $error = $this->_validate($row);
        if(count($error))
        {
            $this->edit($row['id'],$error);
        }
        else
        {
            $row['password'] = md5($row['password']);
            $this->model->update($row);
            header('location: '.Config::URL.$_SESSION['lang'].'/user/index');
        }
        
    }
    
    
    private function _validate($row)
    {
        $error = array();
        
        if (!preg_match("/^[.]{6,20}$/", $row['password']))
        {
            $error['password'] = 'error_password';
        }
        
        return $error;
        
    }
    
}
