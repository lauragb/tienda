<?php

require_once 'smarty/libs/Smarty.class.php';
require_once 'lib/Lang.php';

class View
{
    public $smarty;
    private $_method;
    public $lang;
    public $otherlang;
    
    function __construct()
    {
        $this->lang = new Lang();
        
        //preparar smarty
        $this->smarty = new Smarty();
        $this->smarty->template_dir = 'template';
        $this->smarty->compile_dir = 'tmp';
        
        //asignar variables para las cabeceras/pies
        $this->smarty->assign('lang',$_SESSION['lang']);
        
        if ($_SESSION['lang']=="es"){
            $otherLang = "en";
        }
        else{
            $otherLang = "es";
        }
        
        $this->smarty->assign('other_lang',$otherLang);
        
        
        if (isset($_SESSION['usuario'])){
            $login = 'logout';  
            $user = $_SESSION['usuario'];
            
        }
        else {
            $login = 'login';
            if($_SESSION['lang']=="es"){
               $user = "invitado";   
            }
            else{
                
                $user = "guest";
            }
           /* if(!isset($_SESSION['idRole'])){
               $_SESSION['idRole' = 1;   
            }*/
        }
        
     
        $this->smarty->assign('rol',$_SESSION['idRole']);
        $this->smarty->assign('user',$user);
         $this->smarty->assign('idUser',$_SESSION['idUser']);
        $this->smarty->assign('login',$login);
        $this->smarty->assign('language',$this->lang);
        $this->smarty->assign('url',Config::URL);
    }
    
    
      function getMethod()
    {
        return $this->_method;
    }

    function setMethod($method)
    {
        $this->_method = $method;
    }

}
