<?php

require_once 'lib/Config.php';

abstract class Model 
{
    
    protected $_sql;
    protected $_rows = array();
    private $_connection;

# métodos abstractos para ABM de clases que hereden
    abstract protected function getAll();
    abstract protected function get($number);
    abstract protected function insert($row);
    abstract protected function update($row);
    abstract protected function delete($number);
    
# los siguientes métodos pueden definirse con exactitud
# y no son abstractos
# Conectar a la base de datos
    
    
    public function __construct()
    {
        //echo 'En el Model.php';
    }

    private function openConnection() {
        $this->_connection = new mysqli(Config::DBHOST, Config::DBUSER, Config::DBPASS, Config::DBNAME);
        if ($this->_connection->connect_errno > 0) {
            die("Imposible conectarse con la base de datos [" . $this->_connection->connect_error . "]");
        }
        else{
        }
    }

# Desconectar la base de datos

    private function closeConnection() {
        $this->_connection->close();
    }

# Ejecutar un query simple del tipo INSERT, DELETE, UPDATE

    protected function executeQuery() {
        $this->openConnection();
        $result = $this->_connection->query($this->_sql);
        if ($this->_connection->errno) {
            die($this->_connection->error);
        }
        $result = $this->_connection->insert_id;
        $this->closeConnection();
        return $result;               
    }

# Traer resultados de una consulta en un Array

    protected function executeSelect() 
    {
        $this->openConnection();
        $result = $this->_connection->query($this->_sql);
        while ($this->_rows[] = $result->fetch_assoc());
        $result->close();
        $this->closeConnection();
        array_pop($this->_rows);
    }
    

}
