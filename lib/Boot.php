<?php

require_once 'smarty/libs/Smarty.class.php'; 

require_once 'lib/SecureController.php';

class Boot
{
    
    function __construct()
    {
        //error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
        error_reporting (E_ERROR | E_WARNING);

        $url = $this->_getUrl();
        
        $lang = array_shift($url);
        session_start();
        $this->_setLanguage($lang);
        if(!isset($_SESSION['idRole']))
        {
           $_SESSION['idRole']=1;
           //$_SESSION['usuario']='Anonymous';
        }
        

        
        try{
            $this->_loadController($url);
        } catch (Exception $ex) {
           $this->_dealError($ex);
        }
    }
    

    
    /*FALTA REVISAR EL CÓDIGO DE RAFA Y ARREGLAR ESTE*/
    /**********************************************/
        /**********************************************/
        /**********************************************/
        /**********************************************/
        /**********************************************/
    
    private function _loadController($url)
    {
        $controller = ucfirst($url[0]);
        $fileController = 'controller/' . $controller . '.php';

        if (!file_exists($fileController)) //Por si entramos a un fichero que no existe
        {
            throw new Exception('El controlador no existe',404);
        }
                            
        require_once $fileController;

        $app = new $controller; //Para cargar el controlador
        
        $app = new SecureController($app);
      
        if (isset($url[1])){
            $this->callMethod($url,$app);
        }
        else
        {
            $app->index();
        }

    }
    
    
    private function _getUrl()
    {
         if (isset($_GET['url'])) //tiene que llevar [0]
        {
            $url = $_GET;
            $url = rtrim($_GET['url'],"/");
            //Solo borra las / si quedan a la derecha. Por ejemplo index/
            //Si escribes index/hello no borrará esa / intermedia
            $url = explode("/",$url);
        }
        else
        {
            $url[] = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0,2);
            $url[]= 'index';
        }
        
        return $url;
 
    }
    
    
    private function _setLanguage($lang)
    {
       $file = 'lang/'.$lang.'.php';
       if (!file_exists($file))
       {
           $lang = Config::DEFAULT_LANG;
       }
       
       $_SESSION['lang'] = $lang;
    }
    
    
    private function _dealError($ex)
     {
         $controller = 'Error';
         $fileController = 'controller/' . $controller . '.php';
         require_once $fileController;
         $error = new $controller;
 //            $error->showMessage('Fallo, el controlador no existe <br>');
         $error->view->render($ex);
         exit;        
     }
    
    
    
    
    /*private function exceptionController()
    {
        $controller = 'Error';
        $fileController = 'controller/' . $controller . '.php'; 
        require_once $fileController;
        $error = new $controller;
        $error->view->smarty->assign('message','METODO no encontrado');
        $error->view->render();  
    }*/
    
 
    /* Comprobación de la existencia de método y su invocación con los
     * argumentos pasados en la url
     */
    private function callMethod($url,$app)
    {
        $method = $url[1];
        /*if (!method_exists($app,$method)){
                throw new Exception('El metodo no existe'); /*Hacer la excepción*/
        //} 
        
        switch (count($url))
        {
            case 2:
                $app->$method($url[1]);
                break;
            case 3:
                $app->$method($url[2]);
                break;
            case 4:
                $app->$method($url[2],$url[3]);
                break;

            default:
                $app->$method('Lalalalala');
                break;
        }
     } 
        
}


