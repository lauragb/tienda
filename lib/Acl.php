<?php

/*
 * Clase con los permisos asignados a un rol:
 * Relación ROLE-RESOURCE 
 */


class Acl
{
    
    private $_acl = array(
        'index' => array (
            'index' => 1,
            'hello' => 1
        ),
        'user' => array (
            'index' => 2,
            'addForUser' => 1,
            'addForAdmin' => 3,
            'edit' => 3,
            'delete' => 3,
            'update' => 3,
            'insertForUser' => 1,
            'insertForAdmin' => 3,
        ),
        'products' => array (
            'index' => 1,
            'ajaxPageData' => 1,
            'addToBuy' => 2,
            'ajaxPageFilter' => 1,
            'delete' => 3,
            'update' => 3,
            'insert' => 3
        ),
        'login' => array (
            'index' => 1,
            'login' => 1,
            'run' => 1,
            'logout' => 1
        ),
        'role' => array (
            'getAll' => 3
        ),
        'orders' => array (
            'index' => 3,
            'delete' => 3,
            'update' => 3,
            'newOrder' => 2,
            'insert' => 2,
            'detail'=> 2,
            'getUserOrders' => 2
        ),
        'carrito' => array (
            'index' => 2,
            'delete' => 2,
            'update' => 2
        )
    );
    
    
    function __construct()
    {
    
    }
    
    function isAllowed($className, $method, $accessLevel)
    {
        $className = strtolower($className);
        if (isset($this->_acl[$className][$method])){
            return $accessLevel >= $this->_acl[$className][$method];
        }
        else{
            return true;
        }
        
    }

}