{include file="template/header.tpl" title="header"}
<div id="content">
   <h2>{$language->translate('orders_list')}</h2>
    <table>
        <tr>
            <th>ID</th>
            <th>{$language->translate('order_date')}</th>
            <th>{$language->translate('delivery_date')}</th>
            <th>{$language->translate('status')}</th>
            <th>{$language->translate('user_id')}</th>
            <th>{$language->translate('operations')}</th>
        </tr>
        
        {foreach $rows as $row}
            <tr>
                <td class="centrado">{$row.id}</td>   
                <td  class="centrado">{$row.fechaPedido}</td>   
                <td  class="centrado">{$row.fechaServido}</td>   
                <td  class="centrado">{$row.estado}</td>   
                <td  class="centrado">{$row.idUsuario}</td>  
                <td  class="centrado">
                    <a class="button" href="{$url}{$lang}/orders/detail/{$row.id}">{$language->translate('detail')}</a>
                
                </td>
            </tr> 
        {/foreach}    
    </table>
    
    
</div>
{include file="template/footer.tpl" title="footer"}