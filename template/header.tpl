<!DOCTYPE html>

<html>
    <head>
        <title>TODO: supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="{$url}public/css/default.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="{$url}public/js/jquery-1.10.2.js"></script>
        
        {if isset($js) && count($js)}
            {foreach item=file from=$js}
                <script src="{$url}public/js/{$file}" type="text/javascript"></script>
            {/foreach}
            
        {/if}
        
        <script>
            var rol = "{$rol}";
            
            $(document).ready(function(){
               $("#idioma").on("click",function(e){
                   var url = window.location.pathname;
                   url = url.substring(1);
                   var pathArray = url.split('/'); //Se recoge a partir de tienda/
                   pathArray[1] = "{$other_lang}";
                   pathArray.shift();
                   var newUrl = "{$url}" + pathArray.join('/');
                   $(e.currentTarget).attr("href",newUrl);
               }); 
                
            });
            
            
        </script>
    </head>
    <body>
        <div id="header">
            <div  style="float: left">
                <a href="{$url}{$lang}/index">{$language->translate('index')}</a>
                <a href="{$url}{$lang}/products">{$language->translate('products')}</a>
                {if $rol==2}
                    <a href="{$url}{$lang}/carrito">{$language->translate('basket')}</a>
                {/if}
                {if $rol==2}
                    <a href="{$url}{$lang}/orders/getUserOrders/{$idUser}">{$language->translate('orders')}</a>
                {/if}
                {if $rol==3}
                    <a href="{$url}{$lang}/orders">{$language->translate('orders')}</a>
                {/if}
                {if $rol>2}
                <a href="{$url}{$lang}/user">{$language->translate('user')}</a>
                {/if}
            </div>
            <div  style="float: right">
                <a id="idioma" href="#">{$other_lang}</a>
                {$language->translate('welcome')} {$user}
                <a href="{$url}{$lang}/login/{$login}">{$login}</a> 
                {if $rol==1}
                    <a href="{$url}{$lang}/user/addForUser">{$language->translate('register')}</a>
                {/if}
            </div>
        </div>
