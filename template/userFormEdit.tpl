{include file="template/header.tpl" title="header"}
<div id="content">
    <br>
    <h2 class="titulo">{$language->translate('edit_user')}</h2>
    
    <form action="{$url}{$lang}/user/insert" method="post">
        <label>{$language->translate('name')}</label><input type="text" name="name" value="{$row.nombre}"><br>
        <label>{$language->translate('role')}</label>
            <select name="idRole">
                {foreach $roles as $role}
                <option {($role.id==$row.idRole)?'selected' : ''} value="{$role.id}">
                    {$role.role}
                </option>
                {/foreach}
            </select><br>
        <label>{$language->translate('password')}</label><input type="password" name="password" value="{$row.password}"><br>{$language->translate('error_password')}<br>
        <label></label> <input class="botonEnviar" type="submit" value="{$language->translate('send')}">
    </form>
    
</div>
{include file="template/footer.tpl" title="footer"}