{include file="template/header.tpl" title="header"}
<div id="content">
   <h2>{$language->translate('order_detail')}</h2>
    <table>
        <tr>
            <th>{$language->translate('order_id')}</th>
            <th>{$language->translate('line')}</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('quantity')}</th>
            <th>{$language->translate('price')}</th>
            <th>{$language->translate('subtotal')}</th>
        </tr>
        
        {foreach $rows as $row}
            <tr>
                <td class="centrado">{$row.idPedido}</td>   
                <td  class="centrado">{$row.linea}</td>   
                <td  class="centrado">{$row.nombre}</td>   
                <td  class="centrado">{$row.cantidad}</td>   
                <td  class="centrado">{$row.precio}</td>  
                <td class="centrado2">{math equation="x * y" x=$row.cantidad y=$row.precio}</td>
            </tr> 
        {/foreach}    
    </table>
    <div>
        
    </div>
    <div id="derecha">
        <a class="button" href="{$url}{$lang}/orders/pdf/{$rows[0].idPedido}">{$language->translate('detail')}</a>
    </div>
    
</div>
{include file="template/footer.tpl" title="footer"}