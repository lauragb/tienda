{include file="template/header.tpl" title="header"}
<div id="content">
    <h2>{$language->translate('user_list')}</h2>
    <table>
        <tr>
            <th>ID</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('password')}</th>
            <th>{$language->translate('rol')}</th>
            <th>{$language->translate('operations')}</th>
        </tr>
        
        {foreach $rows as $row}
            <tr>
                <td class="centrado">{$row.id}</td>   
                <td class="centrado">{$row.nombre}</td>   
                <td class="centrado">{$row.password}</td>   
                <td class="centrado">{$row.role}</td>  
                <td class="centrado">
                    <a class="button" href="{$url}{$lang}/user/edit/{$row.id}">{$language->translate('edit')}</a>
                    <a class="button" href="{$url}{$lang}/user/delete/{$row.id}">{$language->translate('delete')}</a>
                </td>
            </tr> 
        {/foreach}    
    </table>
    
    <p><a href="{$url}{$lang}/user/addForAdmin">{$language->translate('new_user')}</a></p>
</div>
{include file="template/footer.tpl" title="footer"}