{include file="template/header.tpl" title="header"}

<div id="content">
      <h2>{$language->translate('products_list')}</h2>
    
    {$language->translate('product_search')}<input id="filtrado" type="text" name="filtrado">
   
    <div id="popup1" class="overlay">
	<div class="popup">
		<h2>Producto añadido correctamente</h2>
		<a class="close" href="#">&times;</a>
	</div>
    </div>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>{$language->translate('code')}</th>
                <th>{$language->translate('name')}</th>
                <th>{$language->translate('price')}</th>
                <th>{$language->translate('stock')}</th>
                {if $rol>1}
                <th>{$language->translate('operations')}</th>
                {/if}
            </tr>
        </thead>
        <tbody id="tBodyList">
        
        </tbody>
         {if $rol==3}
        <tfoot id="tFootList">
             <tr class="cell" id="row' + rows[i].id +'">
                <td class="cell new"><input class="cell" type="text" name="id" disabled></td>
                <td class="cell nuevo new"><input class="cell" type="text" name="codigo" disabled></td>
                <td class="cell nuevo new"><input class="cell" type="text" name="nombre" disabled></td>
                <td class="cell nuevo new"><input class="cell" type="text" name="precio" disabled></td>
                <td class="cell nuevo new"><input class="cell" type="text" name="existencia" disabled></td>
                <td class="cell nuevo new"><a href="#" class="crear">Nuevo</a><span></span></td>
                </tr>
        </tfoot>
        {/if}
    </table>
    
    <div id="paginador">
      
    </div>
    
</div>
{include file="template/footer.tpl" title="footer"}