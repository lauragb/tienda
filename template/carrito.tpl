{include file="template/header.tpl" title="header"}

<script>
    $(document).ready(function(){
        $(".actualizar").on("click",function(e){
          var cantidad = $(e.currentTarget).closest("tr").find(".cantidad").val();
          var id =  $(e.currentTarget).closest("tr").find(".id").val();   
          var url = "{$url}{$lang}/carrito/update/" + id + "/" + cantidad;
            console.log(url);
          $(e.currentTarget).attr("href",url);    
        });
        
    });
    
</script>

<div id="popup2" class="overlay">
	<div class="popup">
		<h2>Pedido añadido correctamente</h2>
		<a class="close" href="#">&times;</a>
	</div>
</div>

<div id="content">
    <h2>{$language->translate('basket')}</h2>
    <table>
        <tr>
           <th>ID</th>
                <th>{$language->translate('name')}</th>
                <th>{$language->translate('price')}</th>
                <th>{$language->translate('quantity')}</th>
                <th>{$language->translate('subtotal')}</th>
                {if $rol>1}
                <th>{$language->translate('operations')}</th>
                {/if}
        </tr>
        
        {foreach $rows as $row}
            <tr class="pedido">
                <td class="cell centrado2"><input class="cell datosPedido id" type="text" name="id" value="{$row.id}" disabled></td>    
                <td class="cell centrado2"><input class="cell datosPedido" type="text" name="nombre" value="{$row.nombre}" disabled></td>   
                <td class="cell centrado2"><input class="cell datosPedido" type="text" name="precio" value="{$row.precio}" disabled></td>  
                <td class="cell centrado2"><input class="cantidad datosPedido" type="number" name="cantidad" value="{$row.cantidad}"></td>
                <td class="cell centrado2"><input class="cell datosPedido" type="number" name="subtotal" value="{math equation="x * y" x=$row.cantidad y=$row.precio}"></td>
                <td class="cell centrado2">
                    
                    <a class="button" href="{$url}{$lang}/carrito/delete/{$row.id}">{$language->translate('delete')}</a>
                    <a class="button actualizar" href="#">{$language->translate('update')}</a>
                </td>
            </tr> 
        {/foreach}    
    </table>

        <p><a id="crearPedido" class="button centrado" href="{$url}{$lang}/orders/newOrder">{$language->translate('save_order')}</a></p>
</div>
{include file="template/footer.tpl" title="footer"}