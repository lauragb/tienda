<?php
require_once 'lib/Model.php';

class OrdersModel extends Model{
    
    function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->_sql = "DELETE FROM pedido WHERE id=$id";
        $this->executeQuery();
    }

    public function get($id)
    {
        $this->_sql = "SELECT * FROM pedido WHERE id =$id";
        $this->executeSelect();
        return $this->_rows[0];
    }

        public function getAll()
    {
        $this->_sql = "SELECT * FROM pedido";
        $this->executeSelect();
        return $this->_rows;
    }
    
        public function getUserOrders($id)
    {
        $this->_sql = "SELECT * FROM pedido where idUsuario=$id";
        $this->executeSelect();
        return $this->_rows;
    }

    public function insert($datos)
    {
    
  
    }
    
      public function insertOrder($productos,$id)
    {
        $lineas = 1;
        
        foreach ($productos as $key => $value) {
            
            $this->_sql = "INSERT INTO detallepedido(cantidad,idPedido,idProducto,precio,linea) "
                . "VALUES ('$value[cantidad]','$id', '$key','$value[precio]','$lineas')";

            $this->executeQuery();
            $lineas++;
        }
  
    }
    
    
    
    public function newOrder($idUser,$fecha)
    {
        $this->_sql = "INSERT INTO pedido(fechaPedido,idUsuario) "
                . "VALUES ('$fecha','$idUser')";

        $lastId = $this->executeQuery();
        return $lastId;
    }
    

    public function update($row)
    {
        $this->_sql = "UPDATE pedido SET "
                . " fechaPedido='$row[fechaPedido]', "
                . " fechaServido='$row[fechaServido]',"
                . " estado=$row[estado],"
                . " idUsuario='$row[idUsuario]'"
                . " WHERE id = $row[id]";
        $this->executeQuery();
    }
    
    public function getDetalle($id)
    {
        $this->_sql = "SELECT d.*,prod.nombre FROM detallepedido d join producto prod on d.idProducto = prod.id WHERE d.idPedido=$id";
        $this->executeSelect();
        return $this->_rows;
    }
    
    public function getDetallePdf($id)
    {
        $this->_sql = "SELECT d.precio,d.cantidad,prod.nombre FROM detallepedido d join producto prod on d.idProducto = prod.id WHERE d.idPedido=$id";
        $this->executeSelect();
        return $this->_rows;
    }

}