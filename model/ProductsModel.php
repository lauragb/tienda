<?php
require_once 'lib/Model.php';

class ProductsModel extends Model{
    
    CONST PAGE_SIZE= 3;
    
    function __construct()
    {
        parent::__construct();
    }

    public function delete($fila)
    {
        $this->_sql = "DELETE FROM producto WHERE id='$fila[id]'";
        $this->executeQuery();
    }

    public function get($id)
    {
        $this->_sql = "SELECT * FROM producto WHERE id =$id";
        $this->executeSelect();
        return $this->_rows[0];
    }
    
        public function getAll()
    {
        $this->_sql = "SELECT * FROM producto";
        $this->executeSelect();
        return $this->_rows;
    }

    public function insert($fila)
    {
        $this->_sql = "INSERT INTO producto(codigo,nombre,precio,existencia) "
                . "VALUES ('$fila[codigo]', '$fila[nombre]', '$fila[precio]', '$fila[existencia]')";

        $this->executeQuery();
    }

    public function update($row)
    {
        $this->_sql = "UPDATE producto SET "
                . " codigo='$row[codigo]', "
                . " nombre='$row[nombre]',"
                . " precio=$row[precio],"
                . " existencia='$row[existencia]'"
                . " WHERE id = $row[id]";
        $this->executeQuery();
    }
    
    public function numeroPaginas()
    {
        $this->_sql= "SELECT CEILING (count(id)/".$this::PAGE_SIZE.") AS pages ".
                "from producto";
        $this->executeSelect();
    }
    
    public function ajaxPageData($pageNumber)
    {
        $this->numeroPaginas();
        $this->_sql = "SELECT * from producto order by id limit "
                . $this::PAGE_SIZE*($pageNumber-1).','.$this::PAGE_SIZE;
        $this->executeSelect();
        return $this->_rows;
    }
    
    
     public function ajaxPageFilter($pageNumber,$text)
     {
        $this->numeroPaginas();
        $this->_sql = "SELECT * from producto where nombre like '".$text."%' order by id limit "
                . $this::PAGE_SIZE*($pageNumber-1).",".$this::PAGE_SIZE;
        $this->executeSelect();
        return $this->_rows;
     }
    
    


}