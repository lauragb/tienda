<?php
/* Smarty version 3.1.30-dev/50, created on 2016-03-11 09:32:49
  from "D:\PHPServer\www\tienda\template\header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30-dev/50',
  'unifunc' => 'content_56e282b19e9031_87341124',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2f66edcc6582ff08df29ccb65b1c081e24fc2c5c' => 
    array (
      0 => 'D:\\PHPServer\\www\\tienda\\template\\header.tpl',
      1 => 1457685167,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56e282b19e9031_87341124 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>

<html>
    <head>
        <title>TODO: supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/css/default.css" type="text/css" rel="stylesheet">
        <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/js/jquery-1.10.2.js"><?php echo '</script'; ?>
>
        
        <?php if (isset($_smarty_tpl->tpl_vars['js']->value) && count($_smarty_tpl->tpl_vars['js']->value)) {?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['js']->value, 'file');
foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->_loop = true;
$__foreach_file_0_saved = $_smarty_tpl->tpl_vars['file'];
?>
                <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
public/js/<?php echo $_smarty_tpl->tpl_vars['file']->value;?>
" type="text/javascript"><?php echo '</script'; ?>
>
            <?php
$_smarty_tpl->tpl_vars['file'] = $__foreach_file_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>
            
        <?php }?>
        
        <?php echo '<script'; ?>
>
            var rol = "<?php echo $_smarty_tpl->tpl_vars['rol']->value;?>
";
            
            $(document).ready(function(){
               $("#idioma").on("click",function(e){
                   var url = window.location.pathname;
                   url = url.substring(1);
                   var pathArray = url.split('/'); //Se recoge a partir de tienda/
                   pathArray[1] = "<?php echo $_smarty_tpl->tpl_vars['other_lang']->value;?>
";
                   pathArray.shift();
                   var newUrl = "<?php echo $_smarty_tpl->tpl_vars['url']->value;?>
" + pathArray.join('/');
                   $(e.currentTarget).attr("href",newUrl);
               }); 
                
            });
            
            
        <?php echo '</script'; ?>
>
    </head>
    <body>
        <div id="header">
            <div  style="float: left">
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/index"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('index');?>
</a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/products"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('products');?>
</a>
                <?php if ($_smarty_tpl->tpl_vars['rol']->value == 2) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/carrito"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('basket');?>
</a>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['rol']->value == 2) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/orders/getUserOrders/<?php echo $_smarty_tpl->tpl_vars['idUser']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('orders');?>
</a>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['rol']->value == 3) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/orders"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('orders');?>
</a>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['rol']->value > 2) {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('user');?>
</a>
                <?php }?>
            </div>
            <div  style="float: right">
                <a id="idioma" href="#"><?php echo $_smarty_tpl->tpl_vars['other_lang']->value;?>
</a>
                <?php echo $_smarty_tpl->tpl_vars['language']->value->translate('welcome');?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value;?>

                <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/login/<?php echo $_smarty_tpl->tpl_vars['login']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['login']->value;?>
</a> 
                <?php if ($_smarty_tpl->tpl_vars['rol']->value == 1) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user/addForUser"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('register');?>
</a>
                <?php }?>
            </div>
        </div>
<?php }
}
