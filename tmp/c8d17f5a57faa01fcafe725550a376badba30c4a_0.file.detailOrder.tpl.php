<?php
/* Smarty version 3.1.30-dev/50, created on 2016-03-11 09:21:39
  from "D:\PHPServer\www\tienda\template\detailOrder.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30-dev/50',
  'unifunc' => 'content_56e28013a81bc4_04988311',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c8d17f5a57faa01fcafe725550a376badba30c4a' => 
    array (
      0 => 'D:\\PHPServer\\www\\tienda\\template\\detailOrder.tpl',
      1 => 1457684497,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_56e28013a81bc4_04988311 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_math')) require_once 'D:\\PHPServer\\www\\tienda\\smarty\\libs\\plugins\\function.math.php';
$_smarty_tpl->_subTemplateRender("file:template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"header"), 0, false);
?>

<div id="content">
   <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('order_detail');?>
</h2>
    <table>
        <tr>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('order_id');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('line');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('name');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('quantity');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('price');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('subtotal');?>
</th>
        </tr>
        
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$__foreach_row_0_saved = $_smarty_tpl->tpl_vars['row'];
?>
            <tr>
                <td class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['idPedido'];?>
</td>   
                <td  class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['linea'];?>
</td>   
                <td  class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['nombre'];?>
</td>   
                <td  class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['cantidad'];?>
</td>   
                <td  class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['precio'];?>
</td>  
                <td class="centrado2"><?php echo smarty_function_math(array('equation'=>"x * y",'x'=>$_smarty_tpl->tpl_vars['row']->value['cantidad'],'y'=>$_smarty_tpl->tpl_vars['row']->value['precio']),$_smarty_tpl);?>
</td>
            </tr> 
        <?php
$_smarty_tpl->tpl_vars['row'] = $__foreach_row_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>    
    </table>
    
    
</div>
<?php $_smarty_tpl->_subTemplateRender("file:template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0, false);
}
}
