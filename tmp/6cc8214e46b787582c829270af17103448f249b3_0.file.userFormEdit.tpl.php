<?php
/* Smarty version 3.1.30-dev/50, created on 2016-03-09 14:23:12
  from "D:\PHPServer\www\tienda\template\userFormEdit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30-dev/50',
  'unifunc' => 'content_56e023c03b7436_82669967',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6cc8214e46b787582c829270af17103448f249b3' => 
    array (
      0 => 'D:\\PHPServer\\www\\tienda\\template\\userFormEdit.tpl',
      1 => 1457529771,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_56e023c03b7436_82669967 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"header"), 0, false);
?>

<div id="content">
    <br>
    <h2 class="titulo"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('edit_user');?>
</h2>
    
    <form action="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user/insert" method="post">
        <label><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('name');?>
</label><input type="text" name="name" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['nombre'];?>
"><br>
        <label><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('role');?>
</label>
            <select name="idRole">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['roles']->value, 'role');
foreach ($_from as $_smarty_tpl->tpl_vars['role']->value) {
$_smarty_tpl->tpl_vars['role']->_loop = true;
$__foreach_role_0_saved = $_smarty_tpl->tpl_vars['role'];
?>
                <option <?php echo $_smarty_tpl->tpl_vars['role']->value['id'] == $_smarty_tpl->tpl_vars['row']->value['idRole'] ? 'selected' : '';?>
 value="<?php echo $_smarty_tpl->tpl_vars['role']->value['id'];?>
">
                    <?php echo $_smarty_tpl->tpl_vars['role']->value['role'];?>

                </option>
                <?php
$_smarty_tpl->tpl_vars['role'] = $__foreach_role_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>
            </select><br>
        <label><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('password');?>
</label><input type="password" name="password" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['password'];?>
"><br><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('error_password');?>
<br>
        <label></label> <input class="botonEnviar" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['language']->value->translate('send');?>
">
    </form>
    
</div>
<?php $_smarty_tpl->_subTemplateRender("file:template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0, false);
}
}
