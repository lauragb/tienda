<?php
/* Smarty version 3.1.30-dev/50, created on 2016-03-09 14:40:20
  from "D:\PHPServer\www\tienda\template\userFormAddAdmin.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30-dev/50',
  'unifunc' => 'content_56e027c4d3bd57_29580274',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '972ad905aa5a55381066698e5b35cc28c3e2fd69' => 
    array (
      0 => 'D:\\PHPServer\\www\\tienda\\template\\userFormAddAdmin.tpl',
      1 => 1457530806,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_56e027c4d3bd57_29580274 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"header"), 0, false);
?>

<div id="content">
    <h2 class="titulo"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('new_user');?>
</h2>
    <?php echo $_smarty_tpl->tpl_vars['message']->value;?>

   <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user/insertForAdmin">
                <label><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('name');?>
</label>
                <input type="text" name="nombre"><br>
       <label><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('role');?>
</label>
            <select name="idRole">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['roles']->value, 'role');
foreach ($_from as $_smarty_tpl->tpl_vars['role']->value) {
$_smarty_tpl->tpl_vars['role']->_loop = true;
$__foreach_role_0_saved = $_smarty_tpl->tpl_vars['role'];
?>
                <option value="<?php echo $_smarty_tpl->tpl_vars['role']->value['id'];?>
">
                    <?php echo $_smarty_tpl->tpl_vars['role']->value['role'];?>

                </option>
                <?php
$_smarty_tpl->tpl_vars['role'] = $__foreach_role_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>
            </select><br>
                <label><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('password');?>
</label>
                <input type="password" name="password"><br>
                <label></label>
       <input class="botonEnviar" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['language']->value->translate('send');?>
">
            </form> 
</div>
<?php $_smarty_tpl->_subTemplateRender("file:template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0, false);
}
}
