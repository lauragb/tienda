<?php
/* Smarty version 3.1.30-dev/50, created on 2016-03-11 00:37:32
  from "D:\PHPServer\www\tienda\template\user.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30-dev/50',
  'unifunc' => 'content_56e2053c9dec71_67418861',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8838a0555e37e9a34dcc1225cf5dc2aa378b227e' => 
    array (
      0 => 'D:\\PHPServer\\www\\tienda\\template\\user.tpl',
      1 => 1457653050,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_56e2053c9dec71_67418861 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"header"), 0, false);
?>

<div id="content">
    <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('user_list');?>
</h2>
    <table>
        <tr>
            <th>ID</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('name');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('password');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('rol');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('operations');?>
</th>
        </tr>
        
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$__foreach_row_0_saved = $_smarty_tpl->tpl_vars['row'];
?>
            <tr>
                <td class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
</td>   
                <td class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['nombre'];?>
</td>   
                <td class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['password'];?>
</td>   
                <td class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['role'];?>
</td>  
                <td class="centrado">
                    <a class="button" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user/edit/<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('edit');?>
</a>
                    <a class="button" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user/delete/<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('delete');?>
</a>
                </td>
            </tr> 
        <?php
$_smarty_tpl->tpl_vars['row'] = $__foreach_row_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>    
    </table>
    
    <p><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/user/addForAdmin"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('new_user');?>
</a></p>
</div>
<?php $_smarty_tpl->_subTemplateRender("file:template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0, false);
}
}
