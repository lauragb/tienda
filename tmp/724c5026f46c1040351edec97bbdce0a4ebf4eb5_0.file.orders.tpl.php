<?php
/* Smarty version 3.1.30-dev/50, created on 2016-03-11 00:39:16
  from "D:\PHPServer\www\tienda\template\orders.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30-dev/50',
  'unifunc' => 'content_56e205a497acd3_95376836',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '724c5026f46c1040351edec97bbdce0a4ebf4eb5' => 
    array (
      0 => 'D:\\PHPServer\\www\\tienda\\template\\orders.tpl',
      1 => 1457653154,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_56e205a497acd3_95376836 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"header"), 0, false);
?>

<div id="content">
   <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('orders_list');?>
</h2>
    <table>
        <tr>
            <th>ID</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('order_date');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('delivery_date');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('status');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('user_id');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('operations');?>
</th>
        </tr>
        
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$__foreach_row_0_saved = $_smarty_tpl->tpl_vars['row'];
?>
            <tr>
                <td class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
</td>   
                <td  class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['fechaPedido'];?>
</td>   
                <td  class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['fechaServido'];?>
</td>   
                <td  class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['estado'];?>
</td>   
                <td  class="centrado"><?php echo $_smarty_tpl->tpl_vars['row']->value['idUsuario'];?>
</td>  
                <td  class="centrado">
                    <a class="button" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/orders/detail/<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('detail');?>
</a>
                
                </td>
            </tr> 
        <?php
$_smarty_tpl->tpl_vars['row'] = $__foreach_row_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>    
    </table>
    
    
</div>
<?php $_smarty_tpl->_subTemplateRender("file:template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0, false);
}
}
