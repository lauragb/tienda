<?php
/* Smarty version 3.1.30-dev/50, created on 2016-03-11 09:09:57
  from "D:\PHPServer\www\tienda\template\carrito.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30-dev/50',
  'unifunc' => 'content_56e27d55ceae07_02454795',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1e77ec9826afb4454fe461c8a0d15a55362896ea' => 
    array (
      0 => 'D:\\PHPServer\\www\\tienda\\template\\carrito.tpl',
      1 => 1457683795,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_56e27d55ceae07_02454795 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_math')) require_once 'D:\\PHPServer\\www\\tienda\\smarty\\libs\\plugins\\function.math.php';
$_smarty_tpl->_subTemplateRender("file:template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"header"), 0, false);
?>


<?php echo '<script'; ?>
>
    $(document).ready(function(){
        $(".actualizar").on("click",function(e){
          var cantidad = $(e.currentTarget).closest("tr").find(".cantidad").val();
          var id =  $(e.currentTarget).closest("tr").find(".id").val();   
          var url = "<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/carrito/update/" + id + "/" + cantidad;
            console.log(url);
          $(e.currentTarget).attr("href",url);    
        });
        
    });
    
<?php echo '</script'; ?>
>

<div id="popup2" class="overlay">
	<div class="popup">
		<h2>Pedido añadido correctamente</h2>
		<a class="close" href="#">&times;</a>
	</div>
</div>

<div id="content">
    <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('basket');?>
</h2>
    <table>
        <tr>
           <th>ID</th>
                <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('name');?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('price');?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('quantity');?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('subtotal');?>
</th>
                <?php if ($_smarty_tpl->tpl_vars['rol']->value > 1) {?>
                <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('operations');?>
</th>
                <?php }?>
        </tr>
        
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['rows']->value, 'row');
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$__foreach_row_0_saved = $_smarty_tpl->tpl_vars['row'];
?>
            <tr class="pedido">
                <td class="cell centrado2"><input class="cell datosPedido id" type="text" name="id" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
" disabled></td>    
                <td class="cell centrado2"><input class="cell datosPedido" type="text" name="nombre" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['nombre'];?>
" disabled></td>   
                <td class="cell centrado2"><input class="cell datosPedido" type="text" name="precio" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['precio'];?>
" disabled></td>  
                <td class="cell centrado2"><input class="cantidad datosPedido" type="number" name="cantidad" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['cantidad'];?>
"></td>
                <td class="cell centrado2"><input class="cell datosPedido" type="number" name="subtotal" value="<?php echo smarty_function_math(array('equation'=>"x * y",'x'=>$_smarty_tpl->tpl_vars['row']->value['cantidad'],'y'=>$_smarty_tpl->tpl_vars['row']->value['precio']),$_smarty_tpl);?>
"></td>
                <td class="cell centrado2">
                    
                    <a class="button" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/carrito/delete/<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('delete');?>
</a>
                    <a class="button actualizar" href="#"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('update');?>
</a>
                </td>
            </tr> 
        <?php
$_smarty_tpl->tpl_vars['row'] = $__foreach_row_0_saved;
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>    
    </table>

        <p><a id="crearPedido" class="button centrado" href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/orders/newOrder"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('save_order');?>
</a></p>
</div>
<?php $_smarty_tpl->_subTemplateRender("file:template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0, false);
}
}
