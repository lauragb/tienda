<?php
/* Smarty version 3.1.30-dev/50, created on 2016-03-10 23:05:07
  from "D:\PHPServer\www\tienda\template\products.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30-dev/50',
  'unifunc' => 'content_56e1ef93be4e24_56518513',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f4bf9fb67d7468b113aa4aeca9b376d0bd7b76e3' => 
    array (
      0 => 'D:\\PHPServer\\www\\tienda\\template\\products.tpl',
      1 => 1457646421,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:template/header.tpl' => 1,
    'file:template/footer.tpl' => 1,
  ),
),false)) {
function content_56e1ef93be4e24_56518513 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"header"), 0, false);
?>


<div id="content">
      <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('products_list');?>
</h2>
    
    <?php echo $_smarty_tpl->tpl_vars['language']->value->translate('product_search');?>
<input id="filtrado" type="text" name="filtrado">
   
    <div id="popup1" class="overlay">
	<div class="popup">
		<h2>Producto añadido correctamente</h2>
		<a class="close" href="#">&times;</a>
	</div>
    </div>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('code');?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('name');?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('price');?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('stock');?>
</th>
                <?php if ($_smarty_tpl->tpl_vars['rol']->value > 1) {?>
                <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('operations');?>
</th>
                <?php }?>
            </tr>
        </thead>
        <tbody id="tBodyList">
        
        </tbody>
         <?php if ($_smarty_tpl->tpl_vars['rol']->value == 3) {?>
        <tfoot id="tFootList">
             <tr class="cell" id="row' + rows[i].id +'">
                <td class="cell new"><input class="cell" type="text" name="id" disabled></td>
                <td class="cell nuevo new"><input class="cell" type="text" name="codigo" disabled></td>
                <td class="cell nuevo new"><input class="cell" type="text" name="nombre" disabled></td>
                <td class="cell nuevo new"><input class="cell" type="text" name="precio" disabled></td>
                <td class="cell nuevo new"><input class="cell" type="text" name="existencia" disabled></td>
                <td class="cell nuevo new"><a href="#" class="crear">Nuevo</a><span></span></td>
                </tr>
        </tfoot>
        <?php }?>
    </table>
    
    <div id="paginador">
      
    </div>
    
</div>
<?php $_smarty_tpl->_subTemplateRender("file:template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0, false);
}
}
